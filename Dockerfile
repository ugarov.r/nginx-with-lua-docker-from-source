FROM debian:9 as build

ENV LUAJIT_LIB=/usr/local/LuaJIT/lib
ENV LUAJIT_INC=/usr/local/LuaJIT/include/luajit-2.0
ENV LD_LIBRARY_PATH=/usr/local/LuaJIT/lib

RUN apt update && apt install -y wget gcc make libpcre3-dev zlib1g-dev
RUN wget http://luajit.org/download/LuaJIT-2.0.5.tar.gz && tar -zxvf LuaJIT-2.0.5.tar.gz && cd LuaJIT-2.0.5 && make && make install PREFIX=/usr/local/LuaJIT
RUN wget https://github.com/simpl/ngx_devel_kit/archive/v0.3.0.tar.gz && tar -zxvf v0.3.0.tar.gz && wget https://github.com/openresty/lua-nginx-module/archive/v0.10.9rc7.tar.gz && tar -zxvf v0.10.9rc7.tar.gz && echo "/usr/local/LuaJIT/lib" >> /etc/ld.so.conf
RUN wget https://nginx.org/download/nginx-1.9.11.tar.gz && tar xvfz nginx-1.9.11.tar.gz && cd nginx-1.9.11 && mkdir modules && ./configure --prefix=/usr/local/nginx --add-module=/lua-nginx-module-0.10.9rc7 --add-module=/ngx_devel_kit-0.3.0 && make && make install


FROM debian:9

ENV LD_LIBRARY_PATH=/usr/local/LuaJIT/lib

WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf ../html /usr/local/LuaJIT/lib && touch ../logs/error.log && touch ../conf/nginx.conf && chmod +x nginx
COPY --from=build /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/mime.types ../conf
COPY --from=build /usr/local/nginx/html/index.html ../html
COPY --from=build /usr/local/LuaJIT/lib/libluajit-5.1.so.2 /usr/local/LuaJIT/lib/
CMD ["./nginx", "-g", "daemon off;"]
